package archetypes.app.person;

import java.util.List;
import java.util.Optional;

public interface PersonRepository {
    List<Person> getAll();

    List<Person> findByName(String name);

    List<Person> findByFilters(PersonFilters filters);

    Optional<Person> findByEmail(String email);

    Person insert(Person person);

    Person update(Person person);

    void delete(long id);
}
