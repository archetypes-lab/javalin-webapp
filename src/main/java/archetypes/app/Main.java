package archetypes.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

  private static final Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {

    log.info("main application start!!!");

    // minimal spring IoC Container
    var spring = new AnnotationConfigApplicationContext();
    spring.scan("archetypes.app");
    spring.refresh();

    // Exposition of the IoC Container
    SpringContext.set(spring);
  }

}
