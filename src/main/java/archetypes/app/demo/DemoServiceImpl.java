package archetypes.app.demo;

import org.springframework.stereotype.Component;

@Component
public class DemoServiceImpl implements DemoService {

    @Override
    public String greetings(String name) {
        return String.format("hello %s from service", name);
    }

}
