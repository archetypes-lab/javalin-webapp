package archetypes.app.permission;

import java.util.List;

public interface PermissionService {

    List<Permission> findByProfileId(Integer id);
    
}
