package archetypes.app.permission;

import java.io.Serializable;

import lombok.Data;

@Data
public class Permission implements Serializable {

    private Integer id;

    private String code;

    private String description;
    
}
