package archetypes.app.util.jwt;

import java.util.Map;

public interface JwtUtils {
    Map<String, Object> parseWithoutKey(String jwt);
}
