package archetypes.app.util.jwt;

import java.util.Map;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

public class JavaJwtUtils implements JwtUtils {

    @Override
    public Map<String, Object> parseWithoutKey(String token) {
        int i = token.lastIndexOf('.');
        String withoutSignature = token.substring(0, i + 1);
        try {
            var jwt = Jwts.parser()
                    .parseClaimsJwt(withoutSignature);
            return jwt.getBody();
        } catch (JwtException ex) {
            throw new IllegalStateException(ex);
        }
    }

}
