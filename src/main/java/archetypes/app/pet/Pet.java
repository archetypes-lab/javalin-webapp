package archetypes.app.pet;

import lombok.Data;

@Data
public class Pet {
    
    private Long id;

    private Long ownerId;

    private String name;

    private String race;

}
