package archetypes.app.pet;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class PetServiceImpl implements PetService {

    private final PetRepository repository;

    public PetServiceImpl(PetRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Pet> findByPersonEmail(String email) {
        return repository.findByPersonEmail(email);
    }

    @Override
    public Pet save(Pet pet) {
        if (pet.getId() == null) {
            return repository.insert(pet);
        } else {
            return repository.update(pet);
        }
    }

    @Override
    public void delete(long idPet) {
        repository.delete(idPet);
    }
    
}
