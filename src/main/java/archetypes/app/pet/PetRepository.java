package archetypes.app.pet;

import java.util.List;

public interface PetRepository {

    List<Pet> findByPersonEmail(String email);

    Pet insert(Pet newPet);

    Pet update(Pet editedPet);

    void delete(long idPet);

}
