package archetypes.app.pet;

import java.util.List;

public interface PetService {

    List<Pet> findByPersonEmail(String email);

    Pet save(Pet pet);

    void delete(long idPet);
    
}
