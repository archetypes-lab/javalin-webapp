package archetypes.app.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import javax.sql.DataSource;

import org.eclipse.jetty.http.HttpCookie.SameSite;
import org.eclipse.jetty.server.session.DatabaseAdaptor;
import org.eclipse.jetty.server.session.JDBCSessionDataStoreFactory;
import org.eclipse.jetty.server.session.NullSessionCache;
import org.eclipse.jetty.server.session.SessionDataStore;
import org.eclipse.jetty.server.session.SessionDataStoreFactory;
import org.eclipse.jetty.server.session.SessionHandler;
import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import archetypes.app.googleapi.RestTemplateOAuth2Api;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import archetypes.app.http.javalin.utils.security.JavalinSecurityUtils;
import archetypes.app.http.javalin.utils.security.JavalinSecurityUtilsImpl;
import archetypes.app.openid.GoogleOpenIdService;
import archetypes.app.openid.OpenIdConfig;
import archetypes.app.openid.OpenIdService;
import archetypes.app.permission.PermissionService;
import archetypes.app.person.PersonService;
import archetypes.app.profile.ProfileService;
import archetypes.app.util.jwt.JavaJwtUtils;
import archetypes.app.util.jwt.JwtUtils;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import io.github.cdimascio.dotenv.Dotenv;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.json.JavalinJackson;
import io.javalin.plugin.rendering.template.JavalinFreemarker;

@org.springframework.context.annotation.Configuration
public class Base {

    // base

    @Bean
    public Dotenv dotenv() {
        return Dotenv.configure().ignoreIfMissing().ignoreIfMalformed().load();
    }

    @Bean
    public Properties mavenProperties() {
        var is = getClass().getClassLoader()
            .getResourceAsStream("properties-from-pom.properties");
        var properties = new Properties();
        try {
            properties.load(is);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        return properties;
    }

    @Bean
    public Boolean prodMode(Dotenv dotenv) {
        var rawProdMode = dotenv.get("PROD_MODE");
        return Boolean.parseBoolean(rawProdMode);
    }

    @Bean
    public ObjectMapper jacksonObjectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule());
    }

    @Bean
    public Configuration freemarkerConfiguration(Boolean prodMode, Properties mavenProperties) {
        var cfg = new freemarker.template.Configuration(Configuration.VERSION_2_3_31);
        cfg.setClassForTemplateLoading(this.getClass(), "/freemarker");

        var sharedVars = new HashMap<String, Object>();
        sharedVars.put("isProdMode", prodMode);
        mavenProperties.entrySet().forEach( entry -> {
            var key = "m_" + entry.getKey().toString().replace(".", "_");
            sharedVars.put(key, entry.getValue());
        });

        try {
            cfg.setSharedVariables(sharedVars);
        } catch (TemplateException ex) {
            throw new IllegalStateException(ex);
        }

        return cfg;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public JwtUtils javaJwtUtils() {
        return new JavaJwtUtils();
    }

    // database

    @Bean
    public DataSource dataSource(Dotenv dotenv) {
        var config = new HikariConfig();
        config.setDriverClassName(dotenv.get("DATABASE_JDBC_DRIVER"));
        config.setJdbcUrl(dotenv.get("DATABASE_JDBC_URL"));
        config.setUsername(dotenv.get("DATABASE_JDBC_USERNAME"));
        config.setPassword(dotenv.get("DATABASE_JDBC_PASSWORD"));
        return new HikariDataSource(config);
    }

    @Bean
    public Object flywayMigration(DataSource dataSource) {
        var flyway = Flyway.configure().dataSource(dataSource).load();
        flyway.migrate();
        return flyway;
    }

    @Bean
    public NamedParameterJdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    // http

    // future for hazelcast
    // https://github.com/hazelcast/hazelcast-jetty-sessionmanager
    @Bean
    public SessionDataStoreFactory jdbcDataStoreFactory(DataSource dataSource) {
        var databaseAdaptor = new DatabaseAdaptor();
        databaseAdaptor.setDatasource(dataSource); // you can set data source here (for connection pooling, etc)
        var jdbcSessionDataStoreFactory = new JDBCSessionDataStoreFactory();
        jdbcSessionDataStoreFactory.setDatabaseAdaptor(databaseAdaptor);
        return jdbcSessionDataStoreFactory;
    }

    @Bean
    public SessionHandler sqlSessionHandler(SessionDataStoreFactory sessionDataStoreFactory) {
        var sessionHandler = new SessionHandler();
        var sessionCache = new NullSessionCache(sessionHandler);
        SessionDataStore sessionDataStore;
        try {
            sessionDataStore = sessionDataStoreFactory.getSessionDataStore(sessionHandler);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        sessionCache.setSessionDataStore(sessionDataStore);
        sessionHandler.setSessionCache(sessionCache);
        sessionHandler.setHttpOnly(true);
        sessionHandler.setSameSite(SameSite.STRICT);
        return sessionHandler;
    }

    @Bean
    @SuppressWarnings("java:S2095")
    public Javalin javalin(
            ObjectMapper jacksonObjectMapper,
            Configuration freemarkerConfiguration,
            SessionHandler sessionHandler) {

        JavalinFreemarker.configure(freemarkerConfiguration);

        var app = Javalin.create(config -> {
            config.addStaticFiles(staticFiles -> {
                staticFiles.hostedPath = "/";
                staticFiles.directory = "/public";
                staticFiles.location = Location.CLASSPATH;
            });
            config.jsonMapper(new JavalinJackson(jacksonObjectMapper));
            config.sessionHandler(() -> sessionHandler);
            config.enableWebjars();
        }).start(8080);
        Runtime.getRuntime().addShutdownHook(new Thread((app::stop)));
        return app;
    }

    @Bean
    public BaseJavalinHandlerConfig javalinHandlerConfig(
            Javalin javalin,
            JavalinSecurityUtils javalinSecurityUtils) {
        return new BaseJavalinHandlerConfig(javalin, javalinSecurityUtils);
    }

    // security

    @Bean
    public OpenIdService openIdService(Dotenv dotenv, RestTemplate restTemplate, JwtUtils jwtUtils) {
        var responseType = "code";
        var scope = "email openid profile";

        var config = new OpenIdConfig(
                responseType,
                scope,
                dotenv.get("LOGIN_OPENID_CLIENT_ID"),
                dotenv.get("LOGIN_OPENID_CLIENT_SECRET"),
                dotenv.get("LOGIN_OPENID_REDIRECT_URL"));

        var oauth2Api = new RestTemplateOAuth2Api(restTemplate);

        return new GoogleOpenIdService(config, oauth2Api, jwtUtils);
    }

    @Bean
    public JavalinSecurityUtils javalinSecurityUtils(
            OpenIdService openIdService,
            PersonService personService,
            ProfileService profileService,
            PermissionService permissionService) {
        return new JavalinSecurityUtilsImpl(openIdService, personService,
                profileService, permissionService);
    }

}
