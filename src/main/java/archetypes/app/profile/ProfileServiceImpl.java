package archetypes.app.profile;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository repository;

    public ProfileServiceImpl(ProfileRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    @Override
    public Profile findById(Integer id) {
        return this.repository.findById(id);
    }

}
