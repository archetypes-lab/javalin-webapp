package archetypes.app.profile;

import java.io.Serializable;

import lombok.Data;

@Data
public class Profile implements Serializable {

    public static final Integer ID_ADMINISTRATOR = 1;
    public static final Integer ID_USER = 2;

    private Integer id;

    private String name;

}
