package archetypes.app.profile;

public interface ProfileService {

    Profile findById(Integer id);
    
}
