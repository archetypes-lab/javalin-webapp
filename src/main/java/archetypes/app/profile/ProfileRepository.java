package archetypes.app.profile;

public interface ProfileRepository {

    Profile findById(Integer id);

}
