package archetypes.app.http.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HttpJsonResponse {

    public enum Status {
        OK,
        BUSINESS_EXCEPTION,
        ERROR
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class JsonExceptionDetail {
        private final String code;

        private final String detail;

        private final Map<String, Object> data;
    }

    private final Status status;

    private final Object payload;

    private final JsonExceptionDetail exception;

}
