package archetypes.app.http.security;

import lombok.Data;

@Data
public class OpenIdCallbackRequest {
    
    private String code;

    private String state;

}
