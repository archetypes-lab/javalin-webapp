package archetypes.app.http.security;

import java.io.Serializable;
import java.util.List;

import archetypes.app.permission.Permission;
import archetypes.app.person.Person;
import archetypes.app.profile.Profile;
import lombok.Data;

@Data
public class Identity implements Serializable {

    private Person person;

    private String pictureUrl;

    private Profile profile;

    private List<Permission> permissions;

}
