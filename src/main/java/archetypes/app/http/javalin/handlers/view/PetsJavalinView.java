package archetypes.app.http.javalin.handlers.view;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import archetypes.app.person.PersonService;
import io.javalin.http.Context;

@Component
public class PetsJavalinView extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(PetsJavalinView.class);

    private final PersonService personService;

    public PetsJavalinView(BaseJavalinHandlerConfig config, PersonService personService) {
        super(config);
        this.personService = personService;
        javalin().get("/pets", this::petsView);
    }

    public void petsView(Context ctx) {
        log.debug("-> enter petsView");
        var email = ctx.queryParam("personEmail");

        var opPerson = this.personService.findByEmail(email);
        if (!opPerson.isPresent()) {
            throw new IllegalStateException(
                String.format("the person with email %s does not exist", email)
            );
        }

        var person = opPerson.get();

        var model = Map.of("email", email, "ownerId", person.getId());
        ctx.render("views/pets.ftl", model);
    }
    
}
