package archetypes.app.http.javalin.handlers.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import io.javalin.http.Context;

@Component
public class HomeJavalinView extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(HomeJavalinView.class);

    public HomeJavalinView(BaseJavalinHandlerConfig config) {
        super(config);
        javalin().get("/", this::homeView);
    }

    public void homeView(Context ctx) {
        log.debug("-> enter homeView");
        ctx.render("views/home.ftl");
    }

}
