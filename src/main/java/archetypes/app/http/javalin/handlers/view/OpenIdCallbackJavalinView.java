package archetypes.app.http.javalin.handlers.view;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import io.javalin.http.Context;

@Component
public class OpenIdCallbackJavalinView extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(OpenIdCallbackJavalinView.class);

    public OpenIdCallbackJavalinView(BaseJavalinHandlerConfig config) {
        super(config);
        javalin().get("/openid-callback", this::openIdCallbackView);
    }

    public void openIdCallbackView(Context ctx) {
        log.debug("-> openIdCallbackView");
        var model = new HashMap<String, Object>();
        model.put("code", ctx.queryParam("code"));
        model.put("state", ctx.queryParam("state"));
        log.debug("model: {}", model);
        ctx.render("views/openidcallback.ftl", model);
    }

}
