package archetypes.app.http.javalin.handlers.api;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.demo.DemoService;
import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import io.javalin.http.Context;

@Component
public class DemoJavalinApi extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(DemoJavalinApi.class);

    private final DemoService demoService;

    public DemoJavalinApi(BaseJavalinHandlerConfig config, DemoService demoService) {
        super(config);
        this.demoService = demoService;

        javalin().routes(() -> {
            path("/api/v1/hello", () -> {
                get("/json/{name}", this::greetingJson);
                get("/{name}", this::greeting);
            });
        });

    }

    public void greeting(Context ctx) {
        log.debug("-> enter greeting");
        var name = ctx.pathParam("name");
        var greetings = this.demoService.greetings(name);
        jsonResponse(ctx, greetings);
    }

    public void greetingJson(Context ctx) {
        log.debug("-> enter greetingJson");
        var name = ctx.pathParam("name");
        var greetings = this.demoService.greetings(name);
        jsonResponse(ctx, greetings);
    }

}
