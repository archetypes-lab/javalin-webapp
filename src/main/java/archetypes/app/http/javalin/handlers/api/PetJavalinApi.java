package archetypes.app.http.javalin.handlers.api;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import archetypes.app.pet.Pet;
import archetypes.app.pet.PetService;
import io.javalin.http.Context;

@Component
public class PetJavalinApi extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(PetJavalinApi.class);

    private final PetService petService;

    public PetJavalinApi(BaseJavalinHandlerConfig config, PetService petService) {
        super(config);
        this.petService = petService;

        javalin().routes(() -> {
            path("/api/v1/pet", () -> {
                post("", this::savePet);
                get("/by-person-email/{email}", this::findByPersonEmail);
                delete("/{id}", this::deletePet);
            });
        });
    }

    public void findByPersonEmail(Context ctx) {
        log.debug("-> enter findByPersonEmail");
        var email = ctx.pathParam("email");
        var pets = this.petService.findByPersonEmail(email);
        jsonResponse(ctx, pets);
    }

    public void savePet(Context ctx) {
        log.debug("-> enter savePet");
        var inputPet = ctx.bodyAsClass(Pet.class);
        this.petService.save(inputPet);
        jsonEmptyResponse(ctx);
    }

    public void deletePet(Context ctx) {
        log.debug("-> enter deletePet");
        var idParam = ctx.pathParam("id");
        var id = Long.parseLong(idParam);
        this.petService.delete(id);
        jsonEmptyResponse(ctx);
    }

}
