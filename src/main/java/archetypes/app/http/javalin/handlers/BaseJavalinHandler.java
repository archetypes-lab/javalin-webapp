package archetypes.app.http.javalin.handlers;

import archetypes.app.http.javalin.utils.security.JavalinSecurityUtils;
import archetypes.app.http.model.HttpJsonResponse;
import archetypes.app.http.model.HttpJsonResponse.Status;
import io.javalin.Javalin;
import io.javalin.http.Context;

public abstract class BaseJavalinHandler {

    private final Javalin javalin;

    private final JavalinSecurityUtils security;

    protected BaseJavalinHandler(BaseJavalinHandlerConfig config) {
        this.javalin = config.getJavalin();
        this.security = config.getSecurity();
    }

    protected Javalin javalin() {
        return javalin;
    }

    protected JavalinSecurityUtils security() {
        return security;
    }

    protected void jsonEmptyResponse(Context ctx) {
        jsonResponse(ctx, null);
    }

    protected void jsonResponse(Context ctx, Object any) {
        var jsonResponse = new HttpJsonResponse(Status.OK, any, null);
        ctx.json(jsonResponse);
    }

}
