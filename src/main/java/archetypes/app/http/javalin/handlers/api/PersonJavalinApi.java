package archetypes.app.http.javalin.handlers.api;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import archetypes.app.person.Person;
import archetypes.app.person.PersonFilters;
import archetypes.app.person.PersonService;
import io.javalin.http.Context;

@Component
public class PersonJavalinApi extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(PersonJavalinApi.class);

    private final PersonService personService;

    public PersonJavalinApi(BaseJavalinHandlerConfig config, PersonService personService) {
        super(config);
        this.personService = personService;

        javalin().routes(() -> {
            path("/api/v1/person", () -> {
                post("", this::savePerson);
                get("/all", this::getAllPersons);
                get("/{id}", this::getPersonById);
                delete("/{id}", this::deletePerson);
            });
        });
    }

    public void savePerson(Context ctx) {
        log.debug("-> enter savePerson");
        var inputPerson = ctx.bodyAsClass(Person.class);
        personService.save(inputPerson);
        jsonEmptyResponse(ctx);
    }

    public void getAllPersons(Context ctx) {
        log.debug("-> enter getAllPerson");
        var persons = personService.getAll();
        jsonResponse(ctx, persons);
    }

    public void getPersonById(Context ctx) {
        log.debug("-> enter getPerson");
        var idParam = ctx.pathParam("id");
        var id = Long.parseLong(idParam);
        var filters = new PersonFilters();
        filters.setId(id);
        var persons = personService.findByFilters(filters);
        var person = persons.isEmpty() ? new Object() : persons.get(0);
        jsonResponse(ctx, person);
    }

    public void deletePerson(Context ctx) {
        log.debug("-> enter deletePerson");
        var idParam = ctx.pathParam("id");
        var id = Long.parseLong(idParam);
        personService.delete(id);
        jsonEmptyResponse(ctx);
    }

}
