package archetypes.app.http.javalin.handlers;

import archetypes.app.http.javalin.utils.security.JavalinSecurityUtils;
import io.javalin.Javalin;
import lombok.Data;

@Data
public class BaseJavalinHandlerConfig {

    private final Javalin javalin;

    private final JavalinSecurityUtils security;

}
