package archetypes.app.http.javalin.handlers.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import io.javalin.http.Context;

@Component
public class DemoJavalinView extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(DemoJavalinView.class);

    public DemoJavalinView(BaseJavalinHandlerConfig config) {
        super(config);
        javalin().get("/demo", this::demoView);
    }

    public void demoView(Context ctx) {
        log.debug("-> enter demoView");
        ctx.render("views/demo.ftl");
    }

}
