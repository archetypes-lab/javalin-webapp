package archetypes.app.http.javalin.handlers.api;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import archetypes.app.http.javalin.handlers.BaseJavalinHandler;
import archetypes.app.http.javalin.handlers.BaseJavalinHandlerConfig;
import archetypes.app.http.security.OpenIdCallbackRequest;
import io.javalin.http.Context;

@Component
public class SecurityJavalinApi extends BaseJavalinHandler {

    private static final Logger log = LoggerFactory.getLogger(SecurityJavalinApi.class);

    public SecurityJavalinApi(BaseJavalinHandlerConfig config) {
        super(config);

        javalin().routes(() -> {
            path("/api/v1/security", () -> {
                get("/openid-url", this::getOpenIdLoginUrl);
                post("/openid-callback", this::receiveOpenIdCallback);
                get("/identity", this::getIdentity);
                get("/logout", this::logout);
            });
        });

    }

    public void getOpenIdLoginUrl(Context ctx) {
        log.debug("-> enter getOpenIdLoginUrl");
        var url = security().getLoginUrl(ctx);
        jsonResponse(ctx, Map.of("url", url));
    }

    public void receiveOpenIdCallback(Context ctx) {
        log.debug("-> enter receiveOpenIdCallback");
        var openIdParams = ctx.bodyAsClass(OpenIdCallbackRequest.class);
        log.debug("openIdParams: {}", openIdParams);
        var identity = security().loginWithOpenIdCode(ctx, openIdParams.getCode(), openIdParams.getState());
        jsonResponse(ctx, identity);
    }

    public void getIdentity(Context ctx) {
        log.debug("-> getIdentity");
        var identity = security().getIdentity(ctx);
        jsonResponse(ctx, identity);
    }

    public void logout(Context ctx) {
        log.debug("-> logout");
        security().logout(ctx);
        jsonEmptyResponse(ctx);
    }

}
