package archetypes.app.http.javalin.utils.security;

import archetypes.app.http.security.Identity;
import io.javalin.http.Context;

public interface JavalinSecurityUtils {

    Identity getIdentity(Context ctx);
    
    String getLoginUrl(Context ctx);

    String getSessionState(Context ctx);

    Identity loginWithOpenIdCode(Context ctx, String code, String state);

    void logout(Context ctx);
    
}
