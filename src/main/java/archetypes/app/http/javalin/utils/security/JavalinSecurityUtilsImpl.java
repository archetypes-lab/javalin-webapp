package archetypes.app.http.javalin.utils.security;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.http.security.Identity;
import archetypes.app.openid.OpenIdService;
import archetypes.app.openid.User;
import archetypes.app.permission.PermissionService;
import archetypes.app.person.Person;
import archetypes.app.person.PersonService;
import archetypes.app.profile.Profile;
import archetypes.app.profile.ProfileService;
import io.javalin.http.Context;

public class JavalinSecurityUtilsImpl implements JavalinSecurityUtils {

    private static final Logger log = LoggerFactory.getLogger(JavalinSecurityUtilsImpl.class);

    private static final String SESSION_STATE_PARAM = "session-state";
    private static final String SESSION_IDENTITY_PARAM = "session-identity";

    private final OpenIdService openIdService;
    private final PersonService personService;
    private final ProfileService profileService;
    private final PermissionService permissionService;

    public JavalinSecurityUtilsImpl(
            OpenIdService openIdService,
            PersonService personService,
            ProfileService profileService,
            PermissionService permissionService) {
        this.openIdService = openIdService;
        this.personService = personService;
        this.profileService = profileService;
        this.permissionService = permissionService;
    }

    @Override
    public Identity getIdentity(Context ctx) {
        return ctx.<Identity>sessionAttribute(SESSION_IDENTITY_PARAM);
    }

    @Override
    public String getLoginUrl(Context ctx) {
        var state = getSessionState(ctx);
        return openIdService.getLoginUrl(state);
    }

    @Override
    public String getSessionState(Context ctx) {
        var state = ctx.<String>sessionAttribute(SESSION_STATE_PARAM);
        if (state == null) {
            state = new BigInteger(130, new SecureRandom()).toString(32);
            ctx.sessionAttribute(SESSION_STATE_PARAM, state);
        }
        return state;
    }

    @Override
    public Identity loginWithOpenIdCode(Context ctx, String code, String state) {
        var actualSessionState = getSessionState(ctx);

        Predicate<String> stateValidator = stateParam -> stateParam != null && stateParam.equals(actualSessionState);

        var openIdUser = this.openIdService.processCallback(code, state, stateValidator);

        log.debug("openIdUser: {}", openIdUser);

        var person = getPersonOrCreateNew(openIdUser);

        var profile = profileService.findById(person.getProfileId());

        var permissions = permissionService.findByProfileId(profile.getId());

        var identity = new Identity();
        identity.setPerson(person);
        identity.setProfile(profile);
        identity.setPermissions(permissions);
        identity.setPictureUrl(openIdUser.getPictureUrl());

        ctx.sessionAttribute(SESSION_IDENTITY_PARAM, identity);

        return identity;
    }

    @Override
    public void logout(Context ctx) {
        ctx.req.getSession().invalidate();
    }

    // private

    private Person getPersonOrCreateNew(User openIdUser) {
        var opPerson = this.personService.findByEmail(openIdUser.getEmail());
        if (opPerson.isPresent()) {
            return opPerson.get();
        }

        // create the person if not exists
        var person = new Person();
        person.setEmail(openIdUser.getEmail());
        person.setFirstName(openIdUser.getFirstName());
        person.setLastName(openIdUser.getLastName());
        person.setProfileId(Profile.ID_USER);
        person.setActive(true);

        return this.personService.save(person);
    }

}
