package archetypes.app.persistence.postgres;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import archetypes.app.pet.Pet;
import archetypes.app.pet.PetRepository;

@Component
@SuppressWarnings({ "java:S1192", "java:S2259" })
public class PetRepoPostgres extends BaseRepoPostgres implements PetRepository {

    private static final RowMapper<Pet> ROW_MAPPER;
    static {
        ROW_MAPPER = new RowMapper<>() {
            @Override
            public Pet mapRow(ResultSet rs, int rowNum) throws SQLException {
                var pet = new Pet();
                pet.setId(rs.getLong("id"));
                pet.setOwnerId(rs.getLong("owner_id"));
                pet.setName(rs.getString("name"));
                pet.setRace(rs.getString("race"));
                return pet;
            }
        };
    }

    public PetRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public List<Pet> findByPersonEmail(String email) {
        var query = """
                select p.* from pet p
                inner join person pe on (p.owner_id = pe.id)
                where pe.email = :email
                    """;
        var params = new MapSqlParameterSource()
            .addValue("email", email);
        return this.jdbcTemplate.query(query, params, ROW_MAPPER);
    }

    @Override
    public Pet insert(Pet newPet) {
        var sql = """
                insert into pet (owner_id, name, race) 
                values (:ownerId, :name, :race)
                """;
        
        var params = new MapSqlParameterSource()
            .addValue("ownerId", newPet.getOwnerId())
            .addValue("name", newPet.getName())
            .addValue("race", newPet.getRace());

        var keyHolder = new GeneratedKeyHolder();
        this.jdbcTemplate.update(sql, params, keyHolder);

        newPet.setId((long) keyHolder.getKeys().get("id"));
        return newPet;
    }

    @Override
    public Pet update(Pet editedPet) {

        var sql = """
                update pet 
                set owner_id=:ownerId, name=:name, race=:race 
                where id=:id
                """;

        var params = new MapSqlParameterSource()
            .addValue("ownerId", editedPet.getOwnerId())
            .addValue("name", editedPet.getName())
            .addValue("race", editedPet.getRace())
            .addValue("id", editedPet.getId());

        this.jdbcTemplate.update(sql, params);

        return editedPet;
    }

    @Override
    public void delete(long idPet) {
        var query = """
                delete from pet
                where id = :id
                """;
        var params = new MapSqlParameterSource()
                .addValue("id", idPet);

        this.jdbcTemplate.update(query, params);
    }

}
