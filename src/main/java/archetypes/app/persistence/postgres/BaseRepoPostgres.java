package archetypes.app.persistence.postgres;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class BaseRepoPostgres {

    protected final NamedParameterJdbcTemplate jdbcTemplate;

    protected BaseRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
}
