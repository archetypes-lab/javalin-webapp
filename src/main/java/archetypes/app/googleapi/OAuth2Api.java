package archetypes.app.googleapi;

public interface OAuth2Api {
    
    OAuth2TokenResponse token(OAuth2TokenRequest request);

}
