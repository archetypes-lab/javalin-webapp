package archetypes.app;

import org.springframework.context.ApplicationContext;

public final class SpringContext {

  private static ApplicationContext context;

  private SpringContext() {
    // empty private constructor
  }

  public static void set(ApplicationContext context) {
    if (SpringContext.context == null) {
      SpringContext.context = context;
    }
  }

  public static <T> T getBean(String name, Class<T> requiredType) {
    return context.getBean(name, requiredType);
  }

  public static <T> T getBean(Class<T> requiredType) {
    return context.getBean(requiredType);
  }

}
