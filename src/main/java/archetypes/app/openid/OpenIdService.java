package archetypes.app.openid;

import java.util.function.Predicate;

public interface OpenIdService {

    String getLoginUrl(String state);

    User processCallback(String code, String state, Predicate<String> stateValidator);
    
}
