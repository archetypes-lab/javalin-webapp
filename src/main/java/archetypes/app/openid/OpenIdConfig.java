package archetypes.app.openid;

import lombok.Data;

@Data
public class OpenIdConfig {
    
    private final String responseType;

    private final String scope;

    private final String clientId;

    private final String clientSecret;

    private final String redirectUri;

}
