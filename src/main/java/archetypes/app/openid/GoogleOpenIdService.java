package archetypes.app.openid;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.googleapi.OAuth2Api;
import archetypes.app.googleapi.OAuth2TokenRequest;
import archetypes.app.util.jwt.JwtUtils;

public class GoogleOpenIdService implements OpenIdService {

    private static final Logger log = LoggerFactory.getLogger(GoogleOpenIdService.class);

    private static final String GOOGLE_ACCOUNTS_HOSTNAME = "https://accounts.google.com/o/oauth2/v2/auth";

    private final OpenIdConfig config;

    private final OAuth2Api oauth2Api;

    private final JwtUtils jwtUtils;

    public GoogleOpenIdService(OpenIdConfig config, OAuth2Api oauth2Api, JwtUtils jwtUtils) {
        this.config = config;
        this.oauth2Api = oauth2Api;
        this.jwtUtils = jwtUtils;
    }

    @Override
    public String getLoginUrl(String state) {
        return new StringBuilder()
                .append(GOOGLE_ACCOUNTS_HOSTNAME).append("?")
                .append("response_type=").append(config.getResponseType())
                .append("&client_id=").append(config.getClientId())
                .append("&scope=").append(URLEncoder.encode(config.getScope(), StandardCharsets.UTF_8))
                .append("&redirect_uri=").append(URLEncoder.encode(config.getRedirectUri(), StandardCharsets.UTF_8))
                .append("&state=").append(state)
                .append("&prompt=consent")
                .toString();
    }

    @Override
    public User processCallback(String code, String state, Predicate<String> stateValidator) {

        var stateValidationSucess = stateValidator.test(state);

        if (!stateValidationSucess) {
            throw new IllegalStateException("invalid google openid state!");
        }

        var tokenRequest = new OAuth2TokenRequest();
        tokenRequest.setCode(code);
        tokenRequest.setClientId(config.getClientId());
        tokenRequest.setClientSecret(config.getClientSecret());
        tokenRequest.setRedirectUri(config.getRedirectUri());
        tokenRequest.setGrantType("authorization_code");

        var token = oauth2Api.token(tokenRequest);

        log.debug("token: {}", token);

        var jwt = jwtUtils.parseWithoutKey(token.getIdToken());

        var user = new User();
        user.setEmail((String) jwt.get("email"));
        user.setFirstName((String) jwt.get("given_name"));
        user.setLastName((String) jwt.get("family_name"));
        user.setPictureUrl((String) jwt.get("picture"));
        user.setLocale((String) jwt.get("locale"));

        return user;
    }

}
