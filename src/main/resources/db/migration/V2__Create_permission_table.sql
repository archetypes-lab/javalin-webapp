CREATE TABLE permission (
	id int4 NOT NULL,
	code varchar(50) NOT NULL,
	description varchar(255) NULL,
	CONSTRAINT permission_pk PRIMARY KEY (id),
	CONSTRAINT permission_code_unique UNIQUE (code)
);

CREATE TABLE permission_profile (
	permission_id int4 NOT NULL,
	profile_id int4 NOT NULL,
	CONSTRAINT permission_profile_pk PRIMARY KEY (permission_id, profile_id),
	CONSTRAINT permission_profile_fk1 FOREIGN KEY (permission_id) REFERENCES permission (id),
	CONSTRAINT permission_profile_fk2 FOREIGN KEY (profile_id) REFERENCES profile (id)
);

INSERT INTO permission (id, code, description) VALUES
(1, 'MANAGE_PERSONS', 'Can create, update and view all persons'),
(2, 'VIEW_OWN_PROFILE', 'Can view her own profile'),
(3, 'MANAGE_OWN_PETS', 'Can manager her own pets'),
(4, 'MANAGE_ALL_PETS', 'Can manager all pets of the system');

INSERT INTO permission_profile (permission_id, profile_id) VALUES 
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(2, 2),
(3, 2);
