<#macro page headExtra="">

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>PrimeVue Demo</title>

  <link rel="icon" type="image/x-icon" href="/img/favicon.ico">

  <link href="/webjars/primevue/${m_wj_primevue_version}/resources/themes/bootstrap4-light-purple/theme.css" rel="stylesheet" />
  <link href="/webjars/primevue/${m_wj_primevue_version}/resources/primevue.min.css" rel="stylesheet" />
  <link href="/webjars/primeicons/${m_wj_primeicons_version}/primeicons.css" rel="stylesheet" />
  <link href="/webjars/primeflex/${m_wj_primeflex_version}/primeflex.css" rel="stylesheet" />
  <link href="/css/main.css" rel="stylesheet" />

  <#if isProdMode> 
    <script src="/webjars/vue/${m_wj_vue_version}/dist/vue.global.prod.js"></script>
  <#else >
    <script src="/webjars/vue/${m_wj_vue_version}/dist/vue.global.js"></script>
  </#if>

  <script src="/webjars/primevue/${m_wj_primevue_version}/core/core.min.js"></script>
  <script src="/webjars/axios/${m_wj_axios_version}/dist/axios.min.js"></script>

  <script src="/js/utils/mapApiResponse.js"></script>
  <script src="/js/api/securityApi.js"></script>
  <script src="/js/service/identityService.js"></script>

  ${headExtra}

</head>
  
<body>
    <div class="mainWrapper">
      <#nested />
    </div>
</body>

</html>

</#macro>
