<#macro head>

    <script src="/webjars/primevue/${m_wj_primevue_version}/menubar/menubar.min.js"></script>

    <script type="module" type="text/javascript">
      import comp1 from "/js/components/headerMenuComponent.js";

      const { createApp, ref } = Vue;

      const HeaderApp = {
        components: {
          "app-header-menu": comp1.headerMenuComponent,
        }
      };

      const app = createApp(HeaderApp);
      app.use(primevue.config.default);
      app.mount("#headerApp");

    </script>

</#macro>

<#macro body>

    <div id="headerApp" class="header">

      <div class="grid">
        <div class="col-12 title">
        Javalin Web Application  
        </div>
      </div>


      <app-header-menu />

    </div>

</#macro>