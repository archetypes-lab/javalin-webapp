<#import "main.ftl" as main>
<#import "includes/header.ftl" as header>
<#import "includes/footer.ftl" as footer>

<#macro page headExtra="" >

    <#assign headExtraContent> 

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,400;0,700;1,200;1,400;1,700&display=swap" rel="stylesheet" />
        <link href="/css/template1.css" rel="stylesheet" />

        <@header.head />

        <@footer.head />

        ${headExtra}

    </#assign>


    <@main.page headExtra=headExtraContent>

        <@header.body />

        <div class="content">
            <#nested />
        </div>

        <@footer.body />

    </@main.page>

</#macro>
