<#import "../macro/template1.ftl" as template1>

<#assign headExtraContent>

  <script src="/webjars/primevue/${m_wj_primevue_version}/slider/slider.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/panel/panel.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/button/button.min.js"></script>

  <script type="module" type="text/javascript">

    import vuecomp from "./js/vue_component.js";

    const { createApp, ref } = Vue;

    const App = {
      setup() {
        const val = ref(null);

        return {
          val,
        };
      },
      components: {
        "p-panel": primevue.panel,
        "p-slider": primevue.slider,
        "c-tap": vuecomp.tapComponent
      },
    };

    createApp(App).use(primevue.config.default).mount("#app");
  </script>

</#assign>

<@template1.page headExtra=headExtraContent>
  <div id="app">

    <p-panel>
      
      <template #header>
        Esto es una prueba!
      </template>

      <p-slider v-model="val"></p-slider>
      <h6>{{val}}</h6>

      <c-tap />

    </p-panel>


  </div>
</@template1.page>
