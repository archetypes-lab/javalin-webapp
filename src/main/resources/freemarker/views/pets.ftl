<#import "../macro/template1.ftl" as template1>

<#assign headExtraContent>

  <script src="/webjars/vue-demi/${m_wj_vuedemi_version}/lib/index.iife.js"></script>
  <script src="/webjars/vuelidate__core/${m_wj_vuelidatecore_version}/dist/index.iife.min.js"></script>
  <script src="/webjars/vuelidate__validators/${m_wj_vuelidatevalidators_version}/dist/index.iife.min.js"></script>

  <script src="/webjars/primevue/${m_wj_primevue_version}/button/button.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/card/card.min.js"></script>
   <script src="/webjars/primevue/${m_wj_primevue_version}/inputtext/inputtext.min.js"></script>

  <script src="/webjars/primevue/${m_wj_primevue_version}/datatable/datatable.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/column/column.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/columngroup/columngroup.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/row/row.min.js"></script>

  <script src="/webjars/primevue/${m_wj_primevue_version}/dialog/dialog.min.js"></script>

  <script src="https://unpkg.com/primevue@^3/confirmdialog/confirmdialog.min.js"></script>
  <script src="https://unpkg.com/primevue@^3/confirmationservice/confirmationservice.min.js"></script>

  <script src="/webjars/primevue/${m_wj_primevue_version}/toast/toast.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/toastservice/toastservice.min.js"></script>

  <script src="/js/api/petApi.js"></script>

  <script>
    const email = '${email}';
    const ownerId = '${ownerId}';
  </script>

  <#noparse>
    <script type="module" type="text/javascript">

      import comp1 from '/js/components/petDialog.js';

      const { createApp, ref } = Vue;

      const App = {
        data: function() {
          return {
            loading: true,
            pets: null
          } 
        },
        components: {
          "p-button": primevue.button,
          "p-card": primevue.card,
          "p-datatable": primevue.datatable,
          "p-column": primevue.column,
          "p-toast": primevue.toast,
          "p-confirmdialog": primevue.confirmdialog,
          "app-pet-dialog": comp1.petDialog
        },
        created: async function() {
          this.pets = await petApi.findPetsByPersonEmail(email);
          this.loading = false;
        },
        methods: {
          async goToNewPet() {
            const toNewPet = {
              ownerId: ownerId 
            }
            await this.$refs.petDlg.open(toNewPet, async (editPet)=> {
              this.pets = await petApi.findPetsByPersonEmail(email);
            });
          },
          async deletePet(pet) {
            this.$confirm.require({
                header: 'Confirmación',
                message: '¿Está seguro de eliminar el registro?',
                icon: 'pi pi-info-circle',
                acceptClass: 'p-button-danger',
                position: 'top',
                accept: async () => {
                  await petApi.deletePet(pet.id);
                  this.pets = await petApi.findPetsByPersonEmail(email);
                  this.$toast.add({severity:'success', summary: 'Éxito', detail:'Se elimino correctamente la mascota', life: 3000});
                }
            });
          },
          async goToEditPet(toEditPet) {
            await this.$refs.petDlg.open(toEditPet, async (toEditPet) => {
              this.pets = await petApi.findPetsByPersonEmail(email);
            });
          }
        }
      };

      createApp(App)
        .use(primevue.config.default)
        .use(primevue.confirmationservice)
        .use(primevue.toastservice)
        .mount("#app");
    </script>

  </#noparse>

</#assign>

<@template1.page headExtra=headExtraContent>
  <div id="app" v-cloak>

    <template v-if="loading">
      Loading...
    </template>
    <template v-else>

      <p-card>

        <template #title >
          Mascotas
        </template>

        <template #content >

          <div class="buttons">
            <p-button label="Nueva Mascota" icon="pi pi-plus" @click="goToNewPet()" ></p-button>
          </div>

          <p-datatable :value="pets" :paginator="true" :rows="10" >
            <p-column field="id" header="Id"></p-column>
            <p-column field="name" header="Nombre"></p-column>
            <p-column field="race" header="Raza"></p-column>
            <p-column header="Acciones">
              <template #body="slotProps">
                <i class="pi pi-trash action" @click="deletePet(slotProps.data)" title="Eliminar"></i>
                <i class="pi pi-pencil action" @click="goToEditPet(slotProps.data)" title="Editar"></i>
              </template>
            </p-column>
            <template #empty>
              No se encontraron mascotas.
            </template>

          </p-datatable>

        </template>

      </p-card>

      <app-pet-dialog ref="petDlg" ></app-pet-dialog>

      <p-confirmdialog></p-confirmdialog>
      <p-toast></p-toast>

    </template>

  </div>
</@template1.page>
