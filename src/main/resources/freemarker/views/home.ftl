<#import "../macro/template1.ftl" as template1>

<#assign headExtraContent>

  <script src="/webjars/primevue/${m_wj_primevue_version}/button/button.min.js"></script>
  <script src="/webjars/primevue/${m_wj_primevue_version}/card/card.min.js"></script>

  <#noparse>
    <script type="module" type="text/javascript">

      const { createApp, ref } = Vue;

      const App = {
        data: function() {
          return {
            loading: true,
            identity: null,
          } 
        },
        components: {
          "p-button": primevue.button,
          "p-card": primevue.card,
        },
        created: async function() {
          const isLoggedIn = await identityService.isLoggedIn();
          if (isLoggedIn) {
            this.identity = await identityService.getIdentity();
          }
          this.loading = false;
        },
        methods: {
          async goToLogin() {
            const data = await securityApi.getOpenIdLoginUrl();
            console.log(`url: ${data.url}`);
            window.location.href = data.url;
          },
          async logout() {
            await securityApi.logout();
            window.location.href = '/';
          }
        }
      };

      createApp(App).use(primevue.config.default).mount("#app");
    </script>
  </#noparse>

</#assign>

<@template1.page headExtra=headExtraContent>
  <div id="app" v-cloak>

    <template v-if="loading">
      Loading...
    </template>
    <template v-else>

      <div class="grid">
        
        <template v-if="identity">
          <div class="col-12 md:col-6 lg:col-4">
            <p-card>
              <template #title>
                Bienvenido {{identity.person.firstName}}
              </template>

              <template #content>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard 
                dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
                It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with 
                desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
              </template>

              <template #footer>
                <p-button v-on:click="logout" label="Cerrar Sesión" />
              </template>
            </p-card>
          </div>
        </template>

        <template v-else>
          <div class="col-12 md:col-6 lg:col-4">
            <p-card>
              <template #title>
                
              </template>

              <template #content>
                Ingresar al sistema...
              </template>

              <template #footer>
                <p-button v-on:click="goToLogin" label="Iniciar Sesión" />
              </template>
            </p-card>
          </div>
           
        </template>

        <div class="col-12 md:col-6 lg:col-4">
          <p-card>
            <template #header>
              <img alt="user header" src="https://www.primefaces.org/primevue/demo/images/usercard.png">
            </template>

            <template #title>
              Advanced Card
            </template>
            
            <template #content>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore sed consequuntur error repudiandae numquam deserunt
                quisquam repellat libero asperiores earum nam nobis, culpa ratione quam perferendis esse, cupiditate neque quas!
            </template>
            
            <template #footer>
              <p-button icon="pi pi-check" label="Save"></p-button>
              <p-button icon="pi pi-times" label="Cancel" class="p-button-secondary" style="margin-left: .5em"></p-button>
            </template>

          </p-card>
        </div>

      </div>

    </template>

  </div>
</@template1.page>
