<#import "../macro/main.ftl" as main>

<#assign headExtraContent>

  <script src="./js/api/securityApi.js"></script>

  <script>
    const code = '${code}';
    const state = '${state}';
  </script>

  <#noparse>
    <script type="module" type="text/javascript">
      import vuecomp from "./js/vue_component.js";

      const { createApp, ref } = Vue;

      const App = {
        mounted: async function() {
            await securityApi.receiveOpenIdCallback(code, state);
            window.location.href = '/';
        }
      };

      createApp(App).use(primevue.config.default).mount("#app");
    </script>
  </#noparse>

</#assign>

<@main.page headExtra=headExtraContent>
  <div id="app">

  </div>
</@main.page>
