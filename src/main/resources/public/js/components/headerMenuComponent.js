import html from "../utils/html.js";

const template = html`
    <p-menubar :model="items">
        
    </p-menubar>
`;

const headerMenuComponent = {
    template: template,
    data() {
        return {
            loading: true,
            items: []
        }
    },
    components: {
        "p-menubar": primevue.menubar,
    },
    created: async function() {
        this.items = await createMenu();
        this.loading = false;
    }
}

export default { headerMenuComponent };

async function createMenu() {

    const menu = [
        {
            label: 'Inicio',
            icon:'pi pi-fw pi-home',
            url: '/'
        }];

    const isLoggedIn = await identityService.isLoggedIn();

    if(!isLoggedIn) {
        menu.push({
            label: 'Iniciar Sesión',
            icon:'pi pi-fw pi-sign-in',
            command: async () => {
                await goToLogin();
            }
        });

    } else {
        
        menu.push({
            label: 'Personas',
            icon:'pi pi-fw pi-folder',
            url: '/persons'
        });

        menu.push({
            label: 'Mascotas',
            icon:'pi pi-fw pi-folder',
            command: async () => {
                await goToPets();
            }
        });

        menu.push({
            label: 'Cerrar Sesión',
            icon:'pi pi-fw pi-sign-out',
            command: async () => {
                await logout();
            } 
        });
    }

    return menu;
}

async function goToLogin() {
    const data = await securityApi.getOpenIdLoginUrl();
    console.log(`url: ${data.url}`);
    window.location.href = data.url;
}

async function logout() {
    await securityApi.logout();
    window.location.href = '/';
}

async function goToPets() {
    const identity = await identityService.getIdentity();
    const email = identity.person.email;
    window.location.href = `/pets?personEmail=${email}`;
}
