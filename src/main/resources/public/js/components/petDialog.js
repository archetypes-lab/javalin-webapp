import html from "../utils/html.js";

const { reactive } = Vue; 
const { useVuelidate } = Vuelidate;
const { required } = VuelidateValidators;
const { useConfirm } = primevue.useconfirm;

const template = html`
    <p-dialog position="top" v-model:visible="state.display" :modal="true" :closable="false"
        :breakpoints="{'960px': '75vw', '640px': '90vw'}" :style="{width: '50vw'}"  >

        <template #header >
            {{ state.editMode ? 'Editar' : 'Nueva'}} Mascota
        </template>

        <form>

            <div class="field">
                <label for="name" :class="{'p-error': v$.name.$error}" >Nombre *</label>
                <p-inputtext id="name" type="text" v-model="state.form.name" 
                    @blur="v$.name.$touch"
                    :class="{'p-error': v$.name.$error}"
                    class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full" ></p-inputtext>
                <div class="input-errors" v-for="error of v$.name.$errors" :key="error.$uid">
                    <small class="p-error">{{ error.$message }}</small>
                </div>
            </div>
            <div class="field">
                <label for="race" :class="{'p-error': v$.race.$error}" >Raza *</label>
                <p-inputtext type="text" v-model="state.form.race"
                    @blur="v$.race.$touch"
                    :class="{'p-error': v$.race.$error}" 
                    class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full" ></p-inputtext>
                <div class="input-errors" v-for="error of v$.race.$errors" :key="error.$uid">
                    <small class="p-error">{{ error.$message }}</small>
                </div>
            </div>

        </form>

        <template #footer >
            <p-button label="Cancelar" icon="pi pi-undo" class="p-button-text" @click="close(v$)" ></p-button>
            <p-button type="submit" label="Aceptar" icon="pi pi-check" @click="handleSubmit(v$)" ></p-button>
        </template>

    </p-dialog>
`;

const petDialog = {
    template: template,
    setup(_props, { expose }) {

        const confirm = useConfirm();

        const state = reactive({
            display: false,
            pet: null,
            editMode: false,
            closeCallback: null,
            form: {
               name: '',
               race: '' 
            }
        });

        const rules = {
            name: { required },
            race: { required }
        };

        const v$ = useVuelidate(rules, state.form);

        const open = async (pet, closeCallback) => {
            state.display = true;
            state.pet = pet;
            state.editMode = state.pet.id != null;
            state.closeCallback = closeCallback
            updateForm(pet);
        };

        expose({
            open: open
        });

        const close = async(inputV$) => {
            state.display = false;

            inputV$.$reset();

            if (state.closeCallback) {
                await state.closeCallback(state.pet);
            }
            
        };

        const handleSubmit = async(inputV$) => {

            inputV$.$touch() 

            if (inputV$.$invalid) {
                console.log('form is not valid!');
                return;
            }

            confirm.require({
                header: 'Confirmación',
                message: '¿Está seguro de guardar el registro?',
                icon: 'pi pi-exclamation-triangle',
                position: 'top',
                accept: async () => {
                    const toSavePet = {
                        id : state.pet.id,
                        ownerId: state.pet.ownerId,
                        name: state.form.name,
                        race: state.form.race
                    };
        
                    await petApi.savePet(toSavePet);
        
                    await close(inputV$);
                }
            });


        }

        const updateForm = (pet) => {
            state.form.name = '';
            state.form.race = '';
            if (pet) {
                state.form.name = pet.name;
                state.form.race = pet.race;
            }
        }

        return { state, v$, open, close, handleSubmit};

    },
    components: {
        'p-dialog': primevue.dialog,
        'p-button': primevue.button,
        'p-inputtext': primevue.inputtext,
    },
}

export default { petDialog };