import myLib from "./mylib.js";
import html from "./utils/html.js";

const template = html`
<li v-for="item in items">
	{{ item }}
</li>
<p-button @click="say">Hola Mundo</p-button>
`;

const tapComponent = {
	template: template,
	components: {
		"p-button": primevue.button,
	},
	data() {
		return {
			items: ['Item1', 'Item2']
		}
	},
	methods: {
		say: myLib.sayHello
	}
}

export default { tapComponent };