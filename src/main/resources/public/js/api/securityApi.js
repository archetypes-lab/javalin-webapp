((global, axios) => {
  global.securityApi = {
    getOpenIdLoginUrl: async function () {
      return axios
        .get('/api/v1/security/openid-url')
        .then((response) => mapApiResponse(response));
    },
    receiveOpenIdCallback: async function (code, state) {
      const openIdCallback = {
        code: code,
        state: state,
      };
      return axios
        .post('/api/v1/security/openid-callback', openIdCallback)
        .then((response) => mapApiResponse(response));
    },
    getIdentity: async function () {
      return axios
        .get('/api/v1/security/identity')
        .then((response) => mapApiResponse(response));
    },
    logout: async function () {
      await axios.get('/api/v1/security/logout');
    },
  };
})(window, axios);
