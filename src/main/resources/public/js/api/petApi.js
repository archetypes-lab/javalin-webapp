((global, axios) => {

    global.petApi = {
        findPetsByPersonEmail: async function (email) {
            return axios
                .get(`/api/v1/pet/by-person-email/${email}`)
                .then(response => mapApiResponse(response));
        },
        savePet: async function (pet) {
            return axios
                .post('/api/v1/pet', pet)
                .then(response => mapApiResponse(response));
        },
        deletePet: async function (id) {
            return axios
                .delete(`/api/v1/pet/${id}`)
                .then(response => mapApiResponse(response));
        }
    };

})(window, axios);
