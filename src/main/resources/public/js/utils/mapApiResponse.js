((global) => {

    global.BusinessError = function(code, detail, data) {
        const error = new Error(detail);
        error.code = code;
        error.data = data;
        return error;
    }

    global.BusinessError.prototype = new Error();

    global.mapApiResponse = function(response) {
        const data = response.data;
        if (data.status == "OK") {
            return data.payload;
        }
        const ex = data.exception;
        if (data.status == "BUSINESS_EXCEPTION") {
            throw new global.BusinessError(ex.code, ex.detail, ex.data);
        } else {
            throw new Error(ex.detail);
        }
    }
})(window);