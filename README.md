# Java Spark Webapp


### Visual Studio Code: Extensions

- **vscjava.vscode-java-pack**: soporte java
- **gabrielbb.vscode-lombok**: soporte libreria lombok
- **dcortes92.freemarker**: soporte plantillas freemarker
- **bierner.lit-html**: soporte html en variable javascript
- **esbenp.prettier-vscode**: formateador html
- **redhat.vscode-xml**: formateador xml
- **streetsidesoftware.code-spell-checker**: corrector ortográfico

