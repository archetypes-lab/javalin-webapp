<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>archetypes</groupId>
  <artifactId>app</artifactId>
  <version>1.0.0-SNAPSHOT</version>

  <properties>
    <app.version>${project.version}</app.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>17</maven.compiler.source>
    <maven.compiler.target>17</maven.compiler.target>

    <maven-shade-plugin.version>3.2.4</maven-shade-plugin.version>
    <maven-surefire-plugin.version>2.22.2</maven-surefire-plugin.version>
    <exec-maven-plugin.version>3.0.0</exec-maven-plugin.version>

    <main-class>archetypes.app.Main</main-class>

    <logback.version>1.2.11</logback.version>
    <spring.version>5.3.20</spring.version>
    <jackson.version>2.13.3</jackson.version>
    <junit-jupiter.version>5.7.0</junit-jupiter.version>

    <wj.axios.version>0.27.2</wj.axios.version>
    <wj.primeflex.version>3.2.1</wj.primeflex.version>
    <wj.primeicons.version>5.0.0</wj.primeicons.version>
    <wj.primevue.version>3.15.0</wj.primevue.version>
    <wj.vue.version>3.2.37</wj.vue.version>
    <wj.vuedemi.version>0.12.5</wj.vuedemi.version>
    <wj.vuelidatecore.version>2.0.0-alpha.42</wj.vuelidatecore.version>
    <wj.vuelidatevalidators.version>2.0.0-alpha.30</wj.vuelidatevalidators.version>

  </properties>

  <dependencies>

    <dependency>
      <groupId>io.javalin</groupId>
      <artifactId>javalin</artifactId>
      <version>4.6.3</version>
    </dependency>

    <dependency>
      <groupId>org.freemarker</groupId>
      <artifactId>freemarker</artifactId>
      <version>2.3.31</version>
    </dependency>

    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>1.18.24</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>

    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.datatype</groupId>
      <artifactId>jackson-datatype-jsr310</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <dependency>
      <groupId>org.flywaydb</groupId>
      <artifactId>flyway-core</artifactId>
      <version>8.5.12</version>
    </dependency>

    <dependency>
      <groupId>io.github.cdimascio</groupId>
      <artifactId>dotenv-java</artifactId>
      <version>2.2.4</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.postgresql</groupId>
      <artifactId>postgresql</artifactId>
      <version>42.4.0</version>
    </dependency>

    <dependency>
      <groupId>com.zaxxer</groupId>
      <artifactId>HikariCP</artifactId>
      <version>5.0.1</version>
    </dependency>

    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>${logback.version}</version>
    </dependency>

    <dependency>
      <groupId>io.jsonwebtoken</groupId>
      <artifactId>jjwt</artifactId>
      <version>0.9.1</version>
    </dependency>

    <dependency>
      <groupId>javax.xml.bind</groupId>
      <artifactId>jaxb-api</artifactId>
      <version>2.3.1</version>
    </dependency>

    <!-- TESTING -->

    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <version>${junit-jupiter.version}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <version>${junit-jupiter.version}</version>
      <scope>test</scope>
    </dependency>

    <!-- WEBJARS -->

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>primevue</artifactId>
      <version>${wj.primevue.version}</version>
    </dependency>

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>primeflex</artifactId>
      <version>${wj.primeflex.version}</version>
    </dependency>

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>primeicons</artifactId>
      <version>${wj.primeicons.version}</version>
    </dependency>

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>axios</artifactId>
      <version>${wj.axios.version}</version>
    </dependency>

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>vue</artifactId>
      <version>${wj.vue.version}</version>
    </dependency>

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>vue-demi</artifactId>
      <version>${wj.vuedemi.version}</version>
    </dependency>

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>vuelidate__validators</artifactId>
      <version>${wj.vuelidatevalidators.version}</version>
    </dependency>

    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>vuelidate__core</artifactId>
      <version>${wj.vuelidatecore.version}</version>
    </dependency>

  </dependencies>

  <build>
    <sourceDirectory>${project.basedir}/src/main/java</sourceDirectory>
    <testSourceDirectory>${project.basedir}/src/test/java</testSourceDirectory>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>${maven-shade-plugin.version}</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <transformers>
                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                  <manifestEntries>
                    <Main-Class>${main-class}</Main-Class>
                  </manifestEntries>
                </transformer>
                <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer" />
              </transformers>
              <outputFile>${project.build.directory}/${project.artifactId}-${project.version}-fat.jar
              </outputFile>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${maven-surefire-plugin.version}</version>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>exec-maven-plugin</artifactId>
        <version>${exec-maven-plugin.version}</version>
        <configuration>
          <mainClass>${main-class}</mainClass>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>properties-maven-plugin</artifactId>
        <version>1.0.0</version>
        <executions>
          <execution>
            <phase>generate-resources</phase>
            <goals>
              <goal>write-project-properties</goal>
            </goals>
            <configuration>
              <outputFile>${project.build.outputDirectory}/properties-from-pom.properties</outputFile>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

  <dependencyManagement>

    <!-- SOLUTIONS TO DEPENDENCIES IN CONFLICT -->

    <dependencies>

      <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
        <version>2.13.3</version>
      </dependency>

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>1.7.36</version>
      </dependency>

    </dependencies>

  </dependencyManagement>

  <repositories>
    <repository>
      <id>repo1</id>
      <url>https://repo1.maven.org/maven2</url>
    </repository>
  </repositories>

</project>