ifeq ($(OS),Windows_NT)
    CURRENT_DIR=$(CD)
	IMAGE_NAME := $(shell basename "$(CD)")
	SSH_PRIVATE_KEY="$$(type ~/.ssh/id_rsa)"
else
	CURRENT_DIR=$(PWD)
	IMAGE_NAME := $(shell basename "$(PWD)")
	SSH_PRIVATE_KEY="$$(cat ~/.ssh/id_rsa)"
endif

dependency-tree:
	./mvnw dependency:tree -Ddetail=true

clean:
	./mvnw clean

compile:
	./mvnw clean compile

build:
	./mvnw clean package

run:
	./mvnw clean compile exec:java -Dexec.mainClass="archetypes.app.Main"

test:
	./mvnw clean test

docker-cicd-build: build
	docker build \
	-f deploy/docker/cicd/Dockerfile \
	--build-arg SSH_PRIVATE_KEY=$(SSH_PRIVATE_KEY) \
	-t archetypes/${IMAGE_NAME}:local-cicd .

docker-cicd-run: docker-cicd-build
	docker run --rm -it -p 8080:8080 \
	--env-file ./.env \
	archetypes/${IMAGE_NAME}:local-cicd
